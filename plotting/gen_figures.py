"""
Copyright (C) 2022 - 2023 J. S. Grewal <rg_public@proton.me>

AGPLv3 Licence:         https://gnu.org/licenses/agpl-3.0.en.html

Codeberg:               https://codeberg.org/raja-grewal
GitHub:                 https://github.com/raja-grewal
LinkedIn:               https://linkedin.com/in/raja-grewal

Title:                  gen_figures.py
Usage:                  python plotting/gen_figures.py

Package Versioning:
    matplotlib          3.7
    scipy               1.10
    torch               1.13

Description:
    Responsible for aggregating data and generating all final summary figures
    in the report for all experiments.

Instructions:
    1. Select experiments to be plotted.
    2. Select additive environment aggregation inputs. Enter into the dictionary
       the common training hyperparameters and then into the lists the features
       that are varied.
    3. Select multiplicative environment aggregation inputs. Enter into each of the
       dictionaries the common training hyperparameters and into the lists the
       appropriate environments.
    4. Select market environment aggregation inputs. Enter into each of the
       dictionaries the common training hyperparameters and into the lists the
       appropriate environments.
    5. Run python file and all figures will be placed inside the `./results/figures`
       directory with duplicate PNG and SVG images provided.
    6. If unsatisfied with graphically displayed growth and leverage values for
       aggregate figures, manual modification and adjustment of some of the plotted
       bounds is possible using the provided lists.
"""

import sys

sys.path.append("./")

import os
from typing import List

import plotting.aggregate_figures as figs
import plotting.plots_misc as msic_plots
from main import gym_envs
from tests.test_input_figures import figure_tests

# select reward dynamics to be plotted
# integer selection using False == 0 and True == 1
PLOT_LEVERAGE = 0
PLOT_ADDITIVE = 0
PLOT_MULTIPLICATIVE = 0
PLOT_MULTIPLICATIVE_SH = 0
PLOT_MARKET = 0
PLOT_GUIDANCE = 0

# only generate figures presented in the report ("Grewal-RLMD.pdf")
ONLY_REPORT_FIGS = 1

# ADDITIVE ENVIRONMENTS

# must select exactly four environments and two algorithms (can repeat selection)
# environment ENV_KEYS from main.gym_envs dictionary
add_envs: List[int] = [12, 13, 15, 7]
# title of plotted environment results
add_name: List[str] = ["Hopper", "Walker", "Ant", "Humanoid"]
add_algos: List[str] = ["SAC", "TD3"]
add_loss: List[str] = [
    "MSE",
    "HUB",
    "MAE",
    "HSC",
    "CAU",
    "TCAU",
    "MSE2",
    "MSE4",
    "MSE6",
]
add_multi: List[int] = [1, 3, 5, 7, 9]

add_inputs: dict = {
    "n_trials": 10,
    "n_cumsteps": 3e5,  # 300,000 training steps
    "eval_freq": 1e3,
    "n_eval": 1e1,
    "buffer": 1e6,
    "critic_mean_type": "E",
    "s_dist": {"SAC": "MVN", "TD3": "N"},
}

# MULTIPLICATIVE ENVIRONMENTS (MARKOV)

# must select exactly three (non-unique) number of simultaneous identical gambles
n_gambles: List[int] = [1, 3, 5]

# must select exactly three (non-unique) environments for each list
# assets following coin flips
coin_keys: List[int] = [18, 19, 20]
# assets following die rolls
dice_keys = [x + 3 for x in coin_keys]
# assets following GBM
gbm_keys = [x + 3 for x in dice_keys]

mul_inputs_td3: dict = {
    "n_trials": 10,
    "n_cumsteps": 1e5,  # 100,000 training steps
    "eval_freq": 1e3,
    "n_eval": 1e3,
    "buffer": 1e6,
    "critic_mean_type": "E",
    "s_dist": "N",
    "algo": "TD3",
    "loss_fn": "MSE",
    "multi_steps": 1,
}

mul_inputs_sac = mul_inputs_td3.copy()
mul_inputs_sac["algo"] = "SAC"
mul_inputs_sac["s_dist"] = "MVN"

# MULTIPLICATIVE INSURANCE SAFE HAVEN ENVIRONMENTS (MARKOV)

# must select exactly two (non-unique) environments for each list
# single asset following the dice roll with safe haven
dice_sh_keys: List[int] = [21, 27]

spitz_inputs_td3 = mul_inputs_td3.copy()
spitz_inputs_sac = mul_inputs_sac.copy()

dice_sh_a_keys: List[int] = [21, 28]
dice_sh_b_keys = [x + 1 for x in dice_sh_a_keys]
dice_sh_c_keys = [x + 1 for x in dice_sh_b_keys]

ins_inputs_td3 = mul_inputs_td3.copy()
ins_inputs_sac = mul_inputs_sac.copy()

# MARKET ENVIRONMENTS (NON-MARKOV)

# must select exactly three (non-unique) number of observed days
obs_days: List[int] = [1, 3, 5]
# action spacing for all market environments
action_days: int = 1

# singular asset time series labels (only for investor A)
mkt_label_name: List[str] = [
    "S&P 500",
    "Gold",
    "Silver",
    "Crude Oil WTI",
    "High Grade Copper",
    "Platinum",
    "Lumber Random Length",
    "Palladium",
    "Gasoline RBOB",
    "Live Cattle",
    "Coffee",
    "Orange Juice",
]

mkt_labels: List[str] = [
    "SPX",
    "GC-F",
    "SI-F",
    "CL-F",
    "HG-F",
    "PL-F",
    "LS-F",
    "PA-F",
    "RB-F",
    "LE-F",
    "KC-F",
    "OJ-F",
]

# multi-asset market environment keys (non-unique)
mkt_name: List[str] = ["USEI", "Minor", "Medium", "Major", "DJI", "Full"]
mkt_invA_env: List[int] = [34, 37, 40, 43]  # , 46, 49]
mkt_invB_env = [x + 1 for x in mkt_invA_env]
mkt_invC_env = [x + 1 for x in mkt_invB_env]

# only obtain multi-asset results for investor A
only_invA = True

mkt_inputs_td3: dict = {
    "n_trials": 10,
    "n_cumsteps": 3e5,  # 300,000 training steps
    "eval_freq": 1e3,
    "n_eval": 1e2,
    "buffer": 1e6,
    "critic_mean_type": "E",
    "s_dist": "N",
    "algo": "TD3",
    "loss_fn": "MSE",
    "multi_steps": 1,
}

mkt_inputs_sac = mkt_inputs_td3.copy()
mkt_inputs_sac["algo"] = "SAC"
mkt_inputs_sac["s_dist"] = "MVN"

if __name__ == "__main__":
    # directory to save figures
    path = "./results/figures/"
    # directory containing leverage experiment data
    leverage_data_path = "./results/multiverse/"
    # directory containing historical market data
    market_data_path = "./tools/market_data/"

    # CONDUCT TESTS
    figure_tests(
        PLOT_LEVERAGE,
        PLOT_ADDITIVE,
        PLOT_MULTIPLICATIVE,
        PLOT_MULTIPLICATIVE_SH,
        PLOT_MARKET,
        PLOT_GUIDANCE,
        ONLY_REPORT_FIGS,
        path,
        leverage_data_path,
        market_data_path,
        gym_envs,
        add_envs,
        add_name,
        add_algos,
        add_loss,
        add_multi,
        add_inputs,
        n_gambles,
        coin_keys,
        dice_keys,
        gbm_keys,
        mul_inputs_td3,
        mul_inputs_sac,
        dice_sh_keys,
        spitz_inputs_td3,
        spitz_inputs_sac,
        dice_sh_a_keys,
        dice_sh_b_keys,
        dice_sh_c_keys,
        ins_inputs_td3,
        ins_inputs_sac,
        obs_days,
        action_days,
        mkt_name,
        mkt_invA_env,
        mkt_invB_env,
        mkt_invC_env,
        only_invA,
        mkt_label_name,
        mkt_labels,
        mkt_inputs_td3,
        mkt_inputs_sac,
    )

    if not os.path.exists(path):
        os.makedirs(path)

    # LEVERAGE EXPERIMENTS

    if PLOT_LEVERAGE:
        # all four leverage experiments
        msic_plots.leverage_plots(leverage_data_path, path)

    # ADDITIVE ENVIRONMENTS

    if PLOT_ADDITIVE:
        # critic loss function plots
        msic_plots.loss_fn_plot(path + "critic_loss")

        figs.plot_additive_critic(
            path, add_envs, add_name, gym_envs, add_inputs, add_algos, add_loss
        )

        figs.plot_additive_multi_step(
            path, add_envs, add_name, gym_envs, add_inputs, add_algos, add_multi
        )

    # MULTIPLICATIVE ENVIRONMENTS (MARKOV)

    if PLOT_MULTIPLICATIVE:
        # action smoothing function plots
        msic_plots.plot_smoothing_fn(path + "action_smoothing")

        # environment: coin flip
        path_env: str = "coin_inv"

        for mul_inputs in [mul_inputs_td3, mul_inputs_sac]:
            algo = "_td3" if mul_inputs == mul_inputs_td3 else "_sac"

            g_min = (  # plotted min growth for each N
                [-4, -20, -20]  # TD3
                if mul_inputs == mul_inputs_td3
                else [-4, -10, -15]  # SAC
            )
            g_max = (  # plotted max growth for each N
                [1, None, 5]  # TD3
                if mul_inputs == mul_inputs_td3
                else [2, 3, 5]  # SAC
            )
            l_min = (  # plotted min leverage for each N
                [-1, -0.5, None]  # TD3
                if mul_inputs == mul_inputs_td3
                else [None, -0.3, -0.3]  # SAC
            )
            l_max = (  # plotted max leverage for each N
                [None, None, 1]  # TD3
                if mul_inputs == mul_inputs_td3
                else [1.5, None, None]  # SAC
            )

            figs.plot_multiplicative_gamble(
                ONLY_REPORT_FIGS,
                path,
                path_env,
                algo,
                gym_envs,
                mul_inputs,
                coin_keys,
                n_gambles,
                g_min,
                g_max,
                l_min,
                l_max,
            )

        # environment: dice roll
        path_env: str = "dice_inv"
        for mul_inputs in [mul_inputs_td3, mul_inputs_sac]:
            algo = "_td3" if mul_inputs == mul_inputs_td3 else "_sac"

            g_min = (  # plotted min growth for each N
                [-4, -10, -20]  # TD3
                if mul_inputs == mul_inputs_td3
                else [-2, -5, -15]  # SAC
            )
            g_max = (  # plotted max growth for each N
                [1, 2, 5]  # TD3
                if mul_inputs == mul_inputs_td3
                else [None, 2, None]  # SAC
            )
            l_min = (  # plotted min leverage for each N
                [None, -0.5, -0.4]  # TD3
                if mul_inputs == mul_inputs_td3
                else [-0.5, -0.5, None]  # SAC
            )
            l_max = (  # plotted max leverage for each N
                [None, 1, None]  # TD3
                if mul_inputs == mul_inputs_td3
                else [None, None, None]  # SAC
            )

            figs.plot_multiplicative_gamble(
                ONLY_REPORT_FIGS,
                path,
                path_env,
                algo,
                gym_envs,
                mul_inputs,
                dice_keys,
                n_gambles,
                g_min,
                g_max,
                l_min,
                l_max,
            )

        #  maximum leverage with GBM plot
        msic_plots.plot_gbm_max_lev(path + "gbm_max_lev")

        # environment: GBM
        path_env: str = "gbm_inv"
        for mul_inputs in [mul_inputs_td3, mul_inputs_sac]:
            algo = "_td3" if mul_inputs == mul_inputs_td3 else "_sac"

            g_min = (  # plotted min growth for each N
                [-5, -5, None]  # TD3
                if mul_inputs == mul_inputs_td3
                else [-5, -5, -5]  # SAC
            )
            g_max = (  # plotted max growth for each N
                [None, None, None]  # TD3
                if mul_inputs == mul_inputs_td3
                else [None, None, None]  # SAC
            )
            l_min = (  # plotted min leverage for each N
                [-1, -1, None]  # TD3
                if mul_inputs == mul_inputs_td3
                else [-2, None, None]  # SAC
            )
            l_max = (  # plotted max leverage for each N
                [None, None, None]  # TD3
                if mul_inputs == mul_inputs_td3
                else [None, None, None]  # SAC
            )

            figs.plot_multiplicative_gamble(
                ONLY_REPORT_FIGS,
                path,
                path_env,
                algo,
                gym_envs,
                mul_inputs,
                gbm_keys,
                n_gambles,
                g_min,
                g_max,
                l_min,
                l_max,
            )

    # MULTIPLICATIVE INSURANCE SAFE HAVEN ENVIRONMENTS (MARKOV)

    if PLOT_MULTIPLICATIVE_SH:
        # environment: dice roll with insurance safe haven
        path_env: str = "dice_sh"

        g_min = [-3, -3]  # plotted min growth for each algorithm
        g_max = [3, 3]  # plotted max growth for each algorithm
        l_min = [-0.25, -0.25]  # plotted min leverage for each algorithm
        l_max = [12 / 11, 12 / 11]  # plotted max leverage for each algorithm

        figs.plot_multiplicative_sh_dice(
            ONLY_REPORT_FIGS,
            path,
            path_env,
            gym_envs,
            spitz_inputs_td3,
            spitz_inputs_sac,
            dice_sh_keys,
            g_min,
            g_max,
            l_min,
            l_max,
        )

        # environment: dice roll with insurance safe haven for investors A-C
        for ins_inputs in [ins_inputs_td3, ins_inputs_sac]:
            algo = "_td3" if ins_inputs == ins_inputs_td3 else "_sac"

            g_min = (  # plotted min growth for each investor
                [-4, -3, -2]  # TD3
                if ins_inputs == ins_inputs_td3
                else [-2, -3, -1]  # SAC
            )
            g_max = (  # plotted max growth for each investor
                [None, None, None]  # TD3
                if ins_inputs == ins_inputs_td3
                else [None, None, None]  # SAC
            )
            l_min = (  # plotted min leverage for each investor
                [-0.5, -1.5, -1]  # TD3
                if ins_inputs == ins_inputs_td3
                else [-0.5, -0.5, -1]  # SAC
            )
            l_max = (  # plotted max leverage for each investor
                [None, None, None]  # TD3
                if ins_inputs == ins_inputs_td3
                else [None, None, None]  # SAC
            )

            figs.plot_multiplicative_sh_inv_dice(
                ONLY_REPORT_FIGS,
                path,
                path_env,
                algo,
                gym_envs,
                ins_inputs,
                dice_sh_a_keys,
                dice_sh_b_keys,
                dice_sh_c_keys,
                g_min,
                g_max,
                l_min,
                l_max,
            )

    # MARKET ENVIRONMENTS (NON-MARKOV)

    if PLOT_MARKET:
        # unique shuffled histories count
        msic_plots.plot_shuffled_histories(path + "shuffled_histories")

        # plot market prices of several assets
        msic_plots.market_prices(market_data_path, path)

        # environment: singular asset agent training
        figs.plot_single_asset(
            ONLY_REPORT_FIGS,
            path,
            gym_envs,
            mkt_inputs_td3,
            mkt_inputs_sac,
            obs_days,
            action_days,
            mkt_label_name,
            mkt_labels,
        )

        # environment: multi-asset agent training
        figs.plot_market(
            ONLY_REPORT_FIGS,
            path,
            gym_envs,
            mkt_inputs_td3,
            mkt_inputs_sac,
            obs_days,
            action_days,
            mkt_name,
            mkt_invA_env,
            mkt_invB_env,
            mkt_invC_env,
            only_invA,
        )

    # GUIDANCE ENVIRONMENTS (NON-MARKOV)

    if PLOT_GUIDANCE:
        pass
