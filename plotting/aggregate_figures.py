"""
Copyright (C) 2022 - 2023 J. S. Grewal <rg_public@proton.me>

AGPLv3 Licence:         https://gnu.org/licenses/agpl-3.0.en.html

Codeberg:               https://codeberg.org/raja-grewal
GitHub:                 https://github.com/raja-grewal
LinkedIn:               https://linkedin.com/in/raja-grewal

Title:                  aggregate_figures.py

Description:
    Collection of functions for plotting final figures presented in a modular format.
"""

import sys

sys.path.append("./")

from os import PathLike
from typing import Dict, List, Union

import plotting.plots_agent as plots
import tools.aggregate_data as aggregate_data


def plot_additive_critic(
    path: Union[str, bytes, PathLike],
    add_envs: List[int],
    add_name: List[str],
    gym_envs: Dict[str, list],
    add_inputs: Dict[str, list],
    add_algos: List[str],
    add_loss: List[str],
) -> None:
    """
    Plot all additive environment figures across critic loss functions.

    Parameters:
        path: path to save
        add_envs: list of environments keys
        add_name: list of environments names
        gym_envs: dictionary of all environment details
        add_inputs: dictionary execution parameters
        add_algos: list of RL algorithms
        add_loss: list of critic loss functions
    """
    path_loss: str = "add_loss"

    loss_data = aggregate_data.add_loss_aggregate(
        add_envs, gym_envs, add_inputs, add_algos, add_loss
    )
    (
        reward,
        closs,
        scale,
        kernel,
        logtemp,
        tail,
        cshadow,
        cmax,
        keqv,
    ) = aggregate_data.add_summary(add_inputs, loss_data)

    plots.plot_add(
        add_inputs,
        add_name,
        add_loss,
        False,
        reward,
        closs,
        scale,
        kernel,
        tail,
        cshadow,
        keqv,
        path + path_loss,
    )

    plots.plot_add_temp(
        add_inputs, add_name, add_loss, False, logtemp, path + path_loss
    )


def plot_additive_multi_step(
    path: Union[str, bytes, PathLike],
    add_envs: List[int],
    add_name: List[str],
    gym_envs: Dict[str, list],
    add_inputs: dict,
    add_algos: List[str],
    add_multi: List[int],
) -> None:
    """
    Plot all additive environment figures across critic multi-steps.

    Parameters:
        path: path to save
        add_envs: list of environments keys
        add_name: list of environments names
        gym_envs: dictionary of all environment details
        add_inputs: dictionary execution parameters
        add_algos: list of RL algorithms
        add_multi: list of critic multi-steps
    """
    path_multi: str = "add_multi"

    multi_data = aggregate_data.add_multi_aggregate(
        add_envs, gym_envs, add_inputs, add_algos, add_multi
    )

    (
        reward,
        closs,
        scale,
        kernel,
        logtemp,
        tail,
        cshadow,
        cmax,
        keqv,
    ) = aggregate_data.add_summary(add_inputs, multi_data)

    plots.plot_add(
        add_inputs,
        add_name,
        add_multi,
        True,
        reward,
        closs,
        scale,
        kernel,
        tail,
        cshadow,
        keqv,
        path + path_multi,
    )

    plots.plot_add_temp(
        add_inputs, add_name, add_multi, True, logtemp, path + path_multi
    )


def plot_multiplicative_gamble(
    ONLY_REPORT_FIGS: bool,
    path: Union[str, bytes, PathLike],
    path_env: Union[str, bytes, PathLike],
    algo: str,
    gym_envs: Dict[str, list],
    mul_inputs: dict,
    gamble_keys: List[int],
    n_gambles: List[int],
    g_min: List[float] = [None, None, None],
    g_max: List[float] = [None, None, None],
    l_min: List[float] = [None, None, None],
    l_max: List[float] = [None, None, None],
) -> None:
    """
    Plot all multiplicative environment figures.

    Parameters:
        ONLY_REPORT_FIGS: only generate figures presented in the report
        path: path to save
        path_env: gamble name
        algo: RL algorithm utilised
        gym_envs: dictionary of all environment details
        mul_inputs: dictionary execution parameters
        gamble_envs: list of environments keys
        g_min: minimum aggregate growth plot limit
        g_max: maximum aggregate growth plot limit
        l_min: minimum aggregate leverage plot limit
        l_max: minimum aggregate leverage plot limit
    """
    # number of simultaneous gambles
    n_str = ["_" + str(n) for n in n_gambles]

    gamble_inv_n1 = aggregate_data.mul_inv_aggregate(
        gamble_keys, n_gambles[0], gym_envs, mul_inputs, safe_haven=False
    )

    (
        reward_1,
        lev_1,
        stop_1,
        reten_1,
        closs_1,
        ctail_1,
        cshadow_1,
        cmax_1,
        keqv_1,
        ls1,
    ) = aggregate_data.mul_inv_n_summary(mul_inputs, gamble_inv_n1)

    gamble_inv_n2 = aggregate_data.mul_inv_aggregate(
        gamble_keys, n_gambles[1], gym_envs, mul_inputs, safe_haven=False
    )

    (
        reward_2,
        lev_2,
        stop_2,
        reten_2,
        closs_2,
        ctail_2,
        cshadow_2,
        cmax_2,
        keqv_2,
        ls2,
    ) = aggregate_data.mul_inv_n_summary(mul_inputs, gamble_inv_n2)

    gamble_inv_n3 = aggregate_data.mul_inv_aggregate(
        gamble_keys, n_gambles[2], gym_envs, mul_inputs, safe_haven=False
    )

    (
        reward_3,
        lev_3,
        stop_3,
        reten_3,
        closs_3,
        ctail_3,
        cshadow_3,
        cmax_3,
        keqv_3,
        ls3,
    ) = aggregate_data.mul_inv_n_summary(mul_inputs, gamble_inv_n3)

    if not ONLY_REPORT_FIGS:
        plots.plot_inv(
            mul_inputs,
            reward_1,
            lev_1,
            stop_1,
            reten_1,
            closs_1,
            ctail_1,
            cshadow_1,
            cmax_1,
            keqv_1,
            path + "mul_" + path_env + algo + n_str[0],
        )

        plots.plot_inv(
            mul_inputs,
            reward_2,
            lev_2,
            stop_2,
            reten_2,
            closs_2,
            ctail_2,
            cshadow_2,
            cmax_2,
            keqv_2,
            path + "mul_" + path_env + algo + n_str[1],
        )

        plots.plot_inv(
            mul_inputs,
            reward_3,
            lev_3,
            stop_3,
            reten_3,
            closs_3,
            ctail_3,
            cshadow_3,
            cmax_3,
            keqv_3,
            path + "mul_" + path_env + algo + n_str[2],
        )

    plots.plot_inv_all_n_perf(
        mul_inputs,
        reward_1,
        lev_1,
        stop_1,
        reten_1,
        reward_2,
        lev_2,
        stop_2,
        reten_2,
        reward_3,
        lev_3,
        stop_3,
        reten_3,
        path + "mul_perf_" + path_env + algo,
        n_gambles,
        g_min,
        g_max,
        l_min,
        l_max,
        T=1,
        V_0=1,
    )

    plots.plot_inv_all_n_train(
        mul_inputs,
        closs_1,
        ctail_1,
        cshadow_1,
        keqv_1,
        closs_2,
        ctail_2,
        cshadow_2,
        keqv_2,
        closs_3,
        ctail_3,
        cshadow_3,
        keqv_3,
        path + "mul_train_" + path_env + algo,
        n_gambles,
    )


def plot_multiplicative_sh_dice(
    ONLY_REPORT_FIGS: bool,
    path: Union[str, bytes, PathLike],
    path_env: Union[str, bytes, PathLike],
    gym_envs: Dict[str, list],
    spitz_inputs_td3: dict,
    spitz_inputs_sac: dict,
    dice_sh_keys: List[int],
    g_min: List[float] = [None, None],
    g_max: List[float] = [None, None],
    l_min: List[float] = [None, None],
    l_max: List[float] = [None, None],
) -> None:
    """
    Plot all multiplicative (safe haven) environment figures.

    Parameters:
        ONLY_REPORT_FIGS: only generate figures presented in the report
        path: path to save
        path_env: gamble name
        gym_envs: dictionary of all environment details
        spitz_inputs_td3: dictionary execution parameters for TD3
        spitz_inputs_sac: dictionary execution parameters for SAC
        dice_sh_keys: list of environments keys
        g_min: minimum aggregate growth plot limit
        g_max: maximum aggregate growth plot limit
        l_min: minimum aggregate leverage plot limit
        l_max: minimum aggregate leverage plot limit
    """
    if not ONLY_REPORT_FIGS:
        for spitz_inputs in [spitz_inputs_td3, spitz_inputs_sac]:
            algo = "_td3" if spitz_inputs == spitz_inputs_td3 else "_sac"

            dice_sh = aggregate_data.mul_inv_aggregate(
                dice_sh_keys, 1, gym_envs, spitz_inputs, safe_haven=True
            )

            (
                reward_sh,
                lev_sh,
                stop_sh,
                reten_sh,
                closs_sh,
                ctail_sh,
                cshadow_sh,
                cmax_sh,
                keqv_sh,
                levsh_sh,
            ) = aggregate_data.mul_inv_n_summary(spitz_inputs, dice_sh)

            plots.plot_safe_haven(
                spitz_inputs,
                reward_sh,
                lev_sh,
                stop_sh,
                reten_sh,
                closs_sh,
                ctail_sh,
                cshadow_sh,
                cmax_sh,
                keqv_sh,
                levsh_sh,
                path + "mul_" + path_env + algo,
                inv="a",
                T=1,
                V_0=1,
            )

    dice_sh_a1 = aggregate_data.mul_inv_aggregate(
        dice_sh_keys, 1, gym_envs, spitz_inputs_sac, safe_haven=True
    )

    dice_sh_a2 = aggregate_data.mul_inv_aggregate(
        dice_sh_keys, 1, gym_envs, spitz_inputs_td3, safe_haven=True
    )

    (
        reward_sh1,
        lev_sh1,
        stop_sh1,
        reten_sh1,
        closs_sh1,
        ctail_1,
        cshadow_sh1,
        cmax_sh1,
        keqv_sh1,
        levsh_sh1,
    ) = aggregate_data.mul_inv_n_summary(spitz_inputs_sac, dice_sh_a1)

    (
        reward_sh2,
        lev_sh2,
        stop_sh2,
        reten_sh2,
        closs_sh2,
        ctail_2,
        cshadow_sh2,
        cmax_sh2,
        keqv_sh2,
        levsh_sh2,
    ) = aggregate_data.mul_inv_n_summary(spitz_inputs_td3, dice_sh_a2)

    plots.plot_sh_perf(
        spitz_inputs_sac,
        reward_sh1,
        lev_sh1,
        reward_sh2,
        lev_sh2,
        path + "mul_perf_" + path_env,
        g_min,
        g_max,
        l_min,
        l_max,
        inv="a",
        T=1,
        V_0=1,
    )

    plots.plot_sh_train(
        spitz_inputs_sac,
        closs_sh1,
        ctail_1,
        cshadow_sh1,
        cmax_sh1,
        keqv_sh1,
        closs_sh2,
        ctail_2,
        cshadow_sh2,
        cmax_sh2,
        keqv_sh2,
        path + "mul_train_" + path_env,
        inv="a",
    )


def plot_multiplicative_sh_inv_dice(
    ONLY_REPORT_FIGS: bool,
    path: Union[str, bytes, PathLike],
    path_env: Union[str, bytes, PathLike],
    algo: str,
    gym_envs: Dict[str, list],
    ins_inputs: dict,
    dice_sh_a_keys: List[int],
    dice_sh_b_keys: List[int],
    dice_sh_c_keys: List[int],
    g_min: List[float] = [None, None, None],
    g_max: List[float] = [None, None, None],
    l_min: List[float] = [None, None, None],
    l_max: List[float] = [None, None, None],
) -> None:
    """
    Plot all multiplicative (safe haven) for investors A-C environment figures.

    Parameters:
        ONLY_REPORT_FIGS: only generate figures presented in the report
        path: path to save
        path_env: gamble name
        algo: RL algorithm utilised
        gym_envs: dictionary of all environment details
        ins_inputs: dictionary execution parameters
        dice_sh_a_keys: list of A environments keys
        dice_sh_b_keys: list of B environments keys
        dice_sh_c_keys: list of C environments keys
        g_min: minimum aggregate growth plot limit
        g_max: maximum aggregate growth plot limit
        l_min: minimum aggregate leverage plot limit
        l_max: minimum aggregate leverage plot limit
    """
    dice_inv_a = aggregate_data.mul_inv_aggregate(
        dice_sh_a_keys, 1, gym_envs, ins_inputs, safe_haven=True
    )

    (
        reward_a,
        lev_a,
        stop_a,
        reten_a,
        closs_a,
        ctail_a,
        cshadow_a,
        cmax_a,
        keqv_a,
        levsh_a,
    ) = aggregate_data.mul_inv_n_summary(ins_inputs, dice_inv_a, safe_haven=True)

    dice_inv_b = aggregate_data.mul_inv_aggregate(
        dice_sh_b_keys, 1, gym_envs, ins_inputs, safe_haven=True
    )

    (
        reward_b,
        lev_b,
        stop_b,
        reten_b,
        closs_b,
        ctail_b,
        cshadow_b,
        ctail_b,
        keqv_b,
        levsh_b,
    ) = aggregate_data.mul_inv_n_summary(ins_inputs, dice_inv_b, safe_haven=True)

    dice_inv_c = aggregate_data.mul_inv_aggregate(
        dice_sh_c_keys, 1, gym_envs, ins_inputs, safe_haven=True
    )

    (
        reward_c,
        lev_c,
        stop_c,
        reten_c,
        closs_c,
        ctail_c,
        cshadow_c,
        ctail_c,
        keqv_c,
        levsh_c,
    ) = aggregate_data.mul_inv_n_summary(ins_inputs, dice_inv_c, safe_haven=True)

    if not ONLY_REPORT_FIGS:
        plots.plot_safe_haven(
            ins_inputs,
            reward_a,
            lev_a,
            stop_a,
            reten_a,
            closs_a,
            ctail_a,
            cshadow_a,
            cmax_a,
            keqv_a,
            levsh_a,
            path + "mul_" + path_env + algo + "_a",
            inv="a",
        )

        plots.plot_safe_haven(
            ins_inputs,
            reward_b,
            lev_b,
            stop_b,
            reten_b,
            closs_b,
            ctail_b,
            cshadow_b,
            ctail_b,
            keqv_b,
            levsh_b,
            inv="b",
        )

        plots.plot_safe_haven(
            ins_inputs,
            reward_c,
            lev_c,
            stop_c,
            reten_c,
            closs_c,
            ctail_c,
            cshadow_c,
            ctail_c,
            keqv_c,
            levsh_c,
            path + "mul_" + path_env + algo + "_c",
            inv="c",
        )

    plots.plot_inv_sh_perf(
        ins_inputs,
        reward_a,
        lev_a,
        stop_a,
        reten_a,
        levsh_a,
        reward_b,
        lev_b,
        stop_b,
        reten_b,
        levsh_b,
        reward_c,
        lev_c,
        stop_c,
        reten_c,
        levsh_c,
        path + "mul_perf_" + path_env + algo,
        g_min,
        g_max,
        l_min,
        l_max,
        T=1,
        V_0=1,
    )

    plots.plot_inv_sh_train(
        ins_inputs,
        closs_a,
        ctail_a,
        cshadow_a,
        keqv_a,
        closs_b,
        ctail_b,
        cshadow_b,
        keqv_b,
        closs_c,
        ctail_c,
        cshadow_c,
        keqv_c,
        path + "mul_train_" + path_env + algo,
    )


def plot_single_asset(
    ONLY_REPORT_FIGS: bool,
    path: Union[str, bytes, PathLike],
    gym_envs: Dict[str, list],
    mkt_inputs_td3: dict,
    mkt_inputs_sac: dict,
    obs_days: List[int],
    action_days: int,
    mkt_label_name: List[str],
    mkt_labels: List[str],
) -> None:
    """
    Plot all single asset market environment figures.

    Parameters:
        ONLY_REPORT_FIGS: only generate figures presented in the report
        path: path to save
        path_env: gamble name
        mkt_inputs_td3: dictionary execution parameters for TD3
        mkt_inputs_sac: dictionary execution parameters for SAC
        obs_days: number of previous observed days
        action_days : number of days between agent actions
        mkt_label_name: list of market names
        mkt_labels: list single asset labels
    """
    if not ONLY_REPORT_FIGS:
        for mkt_inputs in [mkt_inputs_td3, mkt_inputs_sac]:
            algo = "_td3" if mkt_inputs == mkt_inputs_td3 else "_sac"

            e = 0
            for env in mkt_labels:
                equity_inv_o = aggregate_data.mkt_obs_aggregate(
                    env, obs_days, action_days, gym_envs, mkt_inputs, mkt_labels
                )

                (
                    reward,
                    closs,
                    ctail,
                    cshadow,
                    cmax,
                    keqv,
                    mstart,
                    elength,
                ) = aggregate_data.mkt_obs_summary(mkt_inputs, equity_inv_o)

                plots.plot_mkt_inv(
                    mkt_inputs,
                    reward,
                    closs,
                    ctail,
                    cshadow,
                    cmax,
                    keqv,
                    mstart,
                    elength,
                    obs_days,
                    path + "mkt_" + mkt_labels[e] + algo + "_A",
                    bin_size=250,
                )
                e += 1

        e = 0
        for env in mkt_labels:
            equity_inv_a1 = aggregate_data.mkt_obs_aggregate(
                env, obs_days, action_days, gym_envs, mkt_inputs_sac, mkt_labels
            )

            equity_inv_a2 = aggregate_data.mkt_obs_aggregate(
                env, obs_days, action_days, gym_envs, mkt_inputs_td3, mkt_labels
            )

            (
                reward_1,
                closs_1,
                ctail_1,
                cshadow_1,
                cmax_1,
                keqv_1,
                mstart_1,
                elength_1,
            ) = aggregate_data.mkt_obs_summary(mkt_inputs_sac, equity_inv_a1)

            (
                reward_2,
                closs_2,
                ctail_2,
                cshadow_2,
                cmax_2,
                keqv_2,
                mstart_2,
                elength_2,
            ) = aggregate_data.mkt_obs_summary(mkt_inputs_td3, equity_inv_a2)

        plots.plot_mkt_inv_perf(
            mkt_inputs_sac,
            reward_1,
            mstart_1,
            elength_1,
            reward_2,
            mstart_2,
            elength_2,
            obs_days,
            mkt_label_name[e],
            path + "mkt_perf_" + mkt_labels[e] + "_A",
            min_bin_size=90,
            trim_early_perc=10,
        )

        plots.plot_mkt_inv_train(
            mkt_inputs_sac,
            closs_1,
            ctail_1,
            cshadow_1,
            cmax_1,
            keqv_1,
            closs_2,
            ctail_2,
            cshadow_2,
            cmax_2,
            keqv_2,
            obs_days,
            mkt_label_name[e],
            path + "mkt_train_" + mkt_labels[e] + "_A",
        )

        e += 1

    # comparative plot only if number of assets = multiple of 4
    if len(mkt_labels) % 4 == 0:
        page, name = [], []
        page.append(mkt_labels[0:4])
        name.append(mkt_label_name[0:4])

        for x in range(0, int(len(mkt_labels) - 4), 4):
            page.append(mkt_labels[x + 4 : x + 8])
            name.append(mkt_label_name[x + 4 : x + 8])

        algo = ["td3", "sac"]

        a = 0
        for mkt_inputs in [mkt_inputs_td3, mkt_inputs_sac]:
            p = 1
            for envs in page:
                equity_inv_e1 = aggregate_data.mkt_obs_aggregate(
                    envs[0], obs_days, action_days, gym_envs, mkt_inputs, mkt_labels
                )

                equity_inv_e2 = aggregate_data.mkt_obs_aggregate(
                    envs[1], obs_days, action_days, gym_envs, mkt_inputs, mkt_labels
                )

                equity_inv_e3 = aggregate_data.mkt_obs_aggregate(
                    envs[2], obs_days, action_days, gym_envs, mkt_inputs, mkt_labels
                )

                equity_inv_e4 = aggregate_data.mkt_obs_aggregate(
                    envs[3], obs_days, action_days, gym_envs, mkt_inputs, mkt_labels
                )

                (
                    reward_1,
                    closs_1,
                    ctail_1,
                    cshadow_1,
                    cmax_1,
                    keqv_1,
                    mstart_1,
                    elength_1,
                ) = aggregate_data.mkt_obs_summary(mkt_inputs, equity_inv_e1)

                (
                    reward_2,
                    closs_2,
                    ctail_2,
                    cshadow_2,
                    cmax_2,
                    keqv_2,
                    mstart_2,
                    elength_2,
                ) = aggregate_data.mkt_obs_summary(mkt_inputs, equity_inv_e2)

                (
                    reward_3,
                    closs_3,
                    ctail_3,
                    cshadow_3,
                    cmax_3,
                    keqv_3,
                    mstart_3,
                    elength_3,
                ) = aggregate_data.mkt_obs_summary(mkt_inputs, equity_inv_e3)

                (
                    reward_4,
                    closs_4,
                    ctail_4,
                    cshadow_4,
                    cmax_4,
                    keqv_4,
                    mstart_4,
                    elength_4,
                ) = aggregate_data.mkt_obs_summary(mkt_inputs, equity_inv_e4)

                plots.plot_mkt_inv_page_perf(
                    mkt_inputs,
                    reward_1,
                    mstart_1,
                    elength_1,
                    reward_2,
                    mstart_2,
                    elength_2,
                    reward_3,
                    mstart_3,
                    elength_3,
                    reward_4,
                    mstart_4,
                    elength_4,
                    obs_days,
                    name[p - 1],
                    algo[a],
                    path + "mkt_page_sing_" + algo[a] + "_perf_" + str(p) + "_A",
                    min_bin_size=90,
                    trim_early_perc=10,
                    single_asset=True,
                    growth_axis_limit=True,
                    length_axis_limit=True,
                )

                plots.plot_mkt_inv_page_train(
                    mkt_inputs,
                    closs_1,
                    ctail_1,
                    cshadow_1,
                    cmax_1,
                    keqv_1,
                    closs_2,
                    ctail_2,
                    cshadow_2,
                    cmax_2,
                    keqv_2,
                    closs_3,
                    ctail_3,
                    cshadow_3,
                    cmax_3,
                    keqv_3,
                    closs_4,
                    ctail_4,
                    cshadow_4,
                    cmax_4,
                    keqv_4,
                    obs_days,
                    name[p - 1],
                    algo[a],
                    path + "mkt_page_sing_" + algo[a] + "_train_" + str(p) + "_A",
                )

                p += 1
            a += 1


def plot_market(
    ONLY_REPORT_FIGS: bool,
    path: Union[str, bytes, PathLike],
    gym_envs: Dict[str, list],
    mkt_inputs_td3: dict,
    mkt_inputs_sac: dict,
    obs_days: List[int],
    action_days: int,
    mkt_name: List[str],
    mkt_invA_env: List[int],
    mkt_invB_env: List[int],
    mkt_invC_env: List[int],
    only_invA: bool,
) -> None:
    """
    Plot all market environment figures.

    Parameters:
        ONLY_REPORT_FIGS: only generate figures presented in the report
        path: path to save
        path_env: gamble name
        gym_envs: dictionary of all environment details
        mkt_inputs_td3: dictionary execution parameters for TD3
        mkt_inputs_sac: dictionary execution parameters for SAC
        obs_days: number of previous observed days
        action_days : number of days between agent actions
        mkt_name: list of market names
        mkt_invA_env: list of InvA markets
        mkt_invB_env: list of InvB markets
        mkt_invC_env: list of InvC markets
        only_invA: bool if whether only plot InvA
    """
    if not ONLY_REPORT_FIGS:
        inv_str = ["_A", "_B", "_C"]

        mkt_evs = (
            [mkt_invA_env] if only_invA else [mkt_invA_env, mkt_invB_env, mkt_invC_env]
        )

        i = 0
        for invs in mkt_evs:
            for mkt_inputs in [mkt_inputs_td3, mkt_inputs_sac]:
                algo = "_td3" if mkt_inputs == mkt_inputs_td3 else "_sac"

                e = 0
                for env in invs:
                    equity_inv_o = aggregate_data.mkt_obs_aggregate(
                        env, obs_days, action_days, gym_envs, mkt_inputs
                    )

                    (
                        reward,
                        closs,
                        ctail,
                        cshadow,
                        cmax,
                        keqv,
                        mstart,
                        elength,
                    ) = aggregate_data.mkt_obs_summary(mkt_inputs, equity_inv_o)

                    plots.plot_mkt_inv(
                        mkt_inputs,
                        reward,
                        closs,
                        ctail,
                        cshadow,
                        cmax,
                        keqv,
                        mstart,
                        elength,
                        obs_days,
                        path + "mkt_" + mkt_name[e] + algo + inv_str[i],
                        bin_size=250,
                    )
                    e += 1
            i += 1

        i = 0
        for invs in mkt_evs:
            e = 0
            for env in invs:
                equity_inv_a1 = aggregate_data.mkt_obs_aggregate(
                    env, obs_days, action_days, gym_envs, mkt_inputs_sac
                )

                equity_inv_a2 = aggregate_data.mkt_obs_aggregate(
                    env, obs_days, action_days, gym_envs, mkt_inputs_td3
                )

                (
                    reward_1,
                    closs_1,
                    ctail_1,
                    cshadow_1,
                    cmax_1,
                    keqv_1,
                    mstart_1,
                    elength_1,
                ) = aggregate_data.mkt_obs_summary(mkt_inputs_sac, equity_inv_a1)

                (
                    reward_2,
                    closs_2,
                    ctail_2,
                    cshadow_2,
                    cmax_2,
                    keqv_2,
                    mstart_2,
                    elength_2,
                ) = aggregate_data.mkt_obs_summary(mkt_inputs_td3, equity_inv_a2)

                plots.plot_mkt_inv_perf(
                    mkt_inputs_sac,
                    reward_1,
                    mstart_1,
                    elength_1,
                    reward_2,
                    mstart_2,
                    elength_2,
                    obs_days,
                    mkt_name[e],
                    path + "mkt_perf_" + mkt_name[e] + inv_str[i],
                    min_bin_size=90,
                    trim_early_perc=10,
                )

                plots.plot_mkt_inv_train(
                    mkt_inputs_sac,
                    closs_1,
                    ctail_1,
                    cshadow_1,
                    cmax_1,
                    keqv_1,
                    closs_2,
                    ctail_2,
                    cshadow_2,
                    cmax_2,
                    keqv_2,
                    obs_days,
                    mkt_name[e],
                    path + "mkt_train_" + mkt_name[e] + inv_str[i],
                )
                e += 1

            i += 1

    # comparative plot only for investor A and if number of assets = multiple of 4
    if len(mkt_invA_env) % 4 == 0:
        page, name = [], []
        page.append(mkt_invA_env[0:4])
        name.append(mkt_name[0:4])

        for x in range(0, int(len(mkt_invA_env) - 4), 4):
            page.append(mkt_invA_env[x + 4 : x + 8])
            name.append(mkt_name[x + 4 : x + 8])

        algo = ["td3", "sac"]

        a = 0
        for mkt_inputs in [mkt_inputs_td3, mkt_inputs_sac]:
            p = 1
            for envs in page:
                equity_inv_e1 = aggregate_data.mkt_obs_aggregate(
                    envs[0], obs_days, action_days, gym_envs, mkt_inputs
                )

                equity_inv_e2 = aggregate_data.mkt_obs_aggregate(
                    envs[1], obs_days, action_days, gym_envs, mkt_inputs
                )

                equity_inv_e3 = aggregate_data.mkt_obs_aggregate(
                    envs[2], obs_days, action_days, gym_envs, mkt_inputs
                )

                equity_inv_e4 = aggregate_data.mkt_obs_aggregate(
                    envs[3], obs_days, action_days, gym_envs, mkt_inputs
                )

                (
                    reward_1,
                    closs_1,
                    ctail_1,
                    cshadow_1,
                    cmax_1,
                    keqv_1,
                    mstart_1,
                    elength_1,
                ) = aggregate_data.mkt_obs_summary(mkt_inputs, equity_inv_e1)

                (
                    reward_2,
                    closs_2,
                    ctail_2,
                    cshadow_2,
                    cmax_2,
                    keqv_2,
                    mstart_2,
                    elength_2,
                ) = aggregate_data.mkt_obs_summary(mkt_inputs, equity_inv_e2)

                (
                    reward_3,
                    closs_3,
                    ctail_3,
                    cshadow_3,
                    cmax_3,
                    keqv_3,
                    mstart_3,
                    elength_3,
                ) = aggregate_data.mkt_obs_summary(mkt_inputs, equity_inv_e3)

                (
                    reward_4,
                    closs_4,
                    ctail_4,
                    cshadow_4,
                    cmax_4,
                    keqv_4,
                    mstart_4,
                    elength_4,
                ) = aggregate_data.mkt_obs_summary(mkt_inputs, equity_inv_e4)

                plots.plot_mkt_inv_page_perf(
                    mkt_inputs,
                    reward_1,
                    mstart_1,
                    elength_1,
                    reward_2,
                    mstart_2,
                    elength_2,
                    reward_3,
                    mstart_3,
                    elength_3,
                    reward_4,
                    mstart_4,
                    elength_4,
                    obs_days,
                    name[p - 1],
                    algo[a],
                    path + "mkt_page_cfd_" + algo[a] + "_perf_" + str(p) + "_A",
                    min_bin_size=90,
                    trim_early_perc=10,
                    single_asset=False,
                    growth_axis_limit=False,
                    length_axis_limit=False,
                )

                plots.plot_mkt_inv_page_train(
                    mkt_inputs,
                    closs_1,
                    ctail_1,
                    cshadow_1,
                    cmax_1,
                    keqv_1,
                    closs_2,
                    ctail_2,
                    cshadow_2,
                    cmax_2,
                    keqv_2,
                    closs_3,
                    ctail_3,
                    cshadow_3,
                    cmax_3,
                    keqv_3,
                    closs_4,
                    ctail_4,
                    cshadow_4,
                    cmax_4,
                    keqv_4,
                    obs_days,
                    name[p - 1],
                    algo[a],
                    path + "mkt_page_cfd_" + algo[a] + "_train_" + str(p) + "_A",
                )

                p += 1
            a += 1
