"""
Copyright (C) 2022 - 2023 J. S. Grewal <rg_public@proton.me>

AGPLv3 Licence:         https://gnu.org/licenses/agpl-3.0.en.html

Codeberg:               https://codeberg.org/raja-grewal
GitHub:                 https://github.com/raja-grewal
LinkedIn:               https://linkedin.com/in/raja-grewal

Title:                  test_input_market_data.py

Description:
    Responsible for conducting tests on user inputs for market data acquisition.
"""

import sys

sys.path.append("./")

from os import PathLike
from typing import Dict, Union

# default assertion errors
tb: str = "variable must be of type bool"
td: str = "variable must be of type dict"
tf: str = "variable must be of type float"
tfi: str = "variable must be of type float for int"
ti: str = "variable must be of type int"
tl: str = "variable must be of type list"
ts: str = "variable must be of type str"
gte0: str = "quantity must be greater than or equal to 0"
gt0: str = "quantity must be greater than 0"
gte1: str = "quantity must be greater than or equal to 1"


def market_data_tests(
    start: str,
    end: str,
    SAVE_SINGLES: int,
    stooq: Dict[str, list],
    path: Union[str, bytes, PathLike],
    path_singles: Union[str, bytes, PathLike],
    price_type: str,
    price_type_singles: str,
) -> None:
    """
    Conduct tests prior to scrapping historical financial data from Stooq.

    Parameters:
        start: start data
        end: end date
        SAVE_SINGLES: binary of whether to save single asset data
        stooq: market data to download from Stooq
        path: directory to save data
        path_singles: directory to save singles data
        price_type: type of market price to utilise
        price_type_singles: type of market price to utilise for singles
    """
    print(
        "--------------------------------------------------------------------------------"
    )

    assert isinstance(start, str), ts
    assert start[4] == start[7] == "-", "data format must be YYYY-MM-DD"
    assert isinstance(end, str), ts
    assert end[4] == end[7] == "-", "data format must be YYYY-MM-DD"

    y_s, m_s, d_s = start[:4], start[5:7], start[-2:]
    y_e, m_e, d_e = end[:4], end[5:7], end[-2:]

    assert isinstance(int(y_s), int), ti
    assert isinstance(int(m_s), int), ti
    assert isinstance(int(d_s), int), ti
    assert isinstance(int(y_e), int), ti
    assert isinstance(int(m_e), int), ti
    assert isinstance(int(d_e), int), ti

    assert 1900 < int(y_s), "start year should be post 1900"
    assert 0 < int(m_s) <= 12, "only 12 months in year"
    assert 0 < int(d_s) <= 31, "maximum 31 days per month"
    assert 1900 < int(y_s), "end year should be post 1900"
    assert 0 < int(m_e) <= 12, "only 12 months in year"
    assert 0 < int(d_e) <= 31, "maximum 31 days per month"

    assert int(y_e + m_e + d_e) > int(
        y_s + m_s + d_s
    ), "end date must exceed start date"

    assert isinstance(SAVE_SINGLES, int), ti
    assert SAVE_SINGLES == 0 or SAVE_SINGLES == 1, "must be 0 or 1"

    assert isinstance(stooq, dict), td

    for x in stooq:
        mkt = stooq[str(x)]
        assert isinstance(mkt, list), tl
        assert len(mkt) == 2, "mkt must "
        assert isinstance(mkt[0], str), ts
        assert isinstance(mkt[1], list), tl
        assert len(mkt[1]) >= 1, "mkt must contain at least one asset"
        assert len(mkt[1]) == len(set(mkt[1])), "mkt must contain only unique elements"
        assert all(isinstance(a, str) for a in mkt[1]), ts

    assert isinstance(path, Union[str, bytes, PathLike])
    assert (
        path[0:2] == "./" and path[-1] == "/"
    ), "file path must be in a sub-directory relative to main.py"

    assert isinstance(path_singles, Union[str, bytes, PathLike])
    assert (
        path[0:2] == "./" and path[-1] == "/"
    ), "file path must be in a sub-directory relative to main.py"

    assert isinstance(price_type, str)
    assert (
        price_type.capitalize() == "Open" or "High" or "Low" or "Close"
    ), "price_type must be one of Open, High, Low, or Close"

    assert isinstance(price_type_singles, str)
    assert (
        price_type.capitalize() == "Open" or "High" or "Low" or "Close"
    ), "price_type_singles must be one of Open, High, Low, or Close"

    print("Test - Market Data Import Scripts: Passed")

    print(
        "--------------------------------------------------------------------------------"
    )
