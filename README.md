# Reinforcement Learning in Multiplicative Domains

[![License](https://img.shields.io/badge/License-AGPLv3-green)](https://codeberg.org/raja-grewal/rlmd/branch/main/LICENSE)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://pre-commit.com/)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)

Research encompasses several overlapping areas:
1. Learning in multiplicative (non-ergodic) domains, maximising the time-average growth rate,
2. Applications to strongly non-Markovian financial gambles using historical prices,
3. Designing fully autonomous self-learning guidance systems for real-time target acquisition.
4. Peculiarities regarding use of critic loss functions, tail exponents, shadow means, and
5. Multi-step returns and replay buffer coupling in continuous action spaces.

Implementation using [Python](https://www.python.org/) 3.11, [PyTorch](https://pytorch.org) 1.13 with CUDA, and environments interfaced through [Gymnasium](https://gymnasium.farama.org/) 0.27.

## Key Findings

### **Multiplicative Experiments**

* The contemporary methodology prescribed universally by decision theory to increase valuations involves the maximisation of probability-based expectation values, this is inappropriate for multiplicative processes due to conflation of probabilities with payoffs.

* State-of-the-art model-free off-policy reinforcement learning algorithms, initially designed to maximise expected future (additive) rewards, are systematically modified to operate in any conceivable multiplicative environment.

* The model-free agents now fully autonomously self-learn the actions required to maximise any valuation through the avoidance of steep losses, this process is standardised and represented by raising the time-average growth rate.

* The theory is experimentally validated by converging to numerous known analytical optimal growth-maximising actions (leverages) for Markovian gambles involving coin flips, die rolls, and geometric Brownian motion.

* Cost-effective risk mitigation using extremely convex insurance policies are investigated where the agents develop strategies that indisputably increases valuations while simultaneously reducing the amount of risk taken.

* Applications encompass any situation where percentage changes (as opposed to numerical changes) in underlying values are reported, such as financial and economic modelling, medical treatment, supply chain management, and guidance systems.

### **Market Experiments**

* Agents were trained and found to be robustly capable of operating in extremely non-Markovian regimes such as financial markets using only consecutive past daily returns for a large assortment of highly liquid USD-denominated equity indices and commodities.

* Historical time series simulation is entirely non-parametrically performed using shuffled holdout out-of-sample forecasting using 36 years of data (1985/10 - 2022/08) with there existing long-term positive correlation across each simulation.

* Performance across a myriad of complex environments is found to exhibit substantial positive skew in wealth growth whilst being highly dependent on both the historical time-period and the nature of the underlying asset.

* Single-asset portfolios construction is fully compatible with both traditional portfolio and risk management methods while multi-asset portfolios are modelled as trading bundles of zero margin CFDs.

### **Guidance Experiments**

* Created fully automated guidance systems that control the trajectory of point projectiles to targets in unbounded 3D space while under the effects of slowly varying vector fields such as wind.

* Presented the concept of a novel energy efficient multi-stage actors for operation in extremely remote environments and designed countermeasure systems for intercepting projectiles.

### **Additive Experiments**

* Critic loss evaluation using MSE is an acceptable starting point, but use of HUB, MAE, and HSC should be considered as there exists serious potential for ‘free’ performance gains depending
on environment.

* Critic loss mini-batches appear to exhibit extreme kurtosis (fat tails) and so aggregation using 'empirical' arithmetic means (Monte-Carlo approach) severely underestimates the true population mean.

* Multi-step returns for continuous action spaces using TD3 and SAC is not advised due to lack of global policy maximisation across the infinite action space unlike the finite discrete case.

## Data Analysis

Comprehensive discussion and implications of all results are described in the report `Grewal-RLMD.pdf`.

The data regarding optimal leverage experiments (NumPy arrays), agent training performance (NumPy arrays), and the learned models (PyTorch parameters) have a total combined size of 16.9 GB.

The breakdown for optimal leverage experiments, additive agents, multiplicative agents, and market agents are 0.4 GB, 2.5 GB, 7.7 GB, and 6.4 GB respectively. All data is available upon request.

## Usage

Using the latest [release](https://codeberg.org/raja-grewal/rlmd/releases) is recommended where we adhere to [semantic](https://semver.org/) versioning.

Binary coin flip, trinary die roll, and geometric Brownian motion (GBM) experiments pertaining to empirical optimal leverages are contained in the `lev/` directory with instructions provided inside each of the files.

Training on market environments requires historical data sourced from [Stooq](https://stooq.com/) generated using `tools/gen_market_data.py` with customisation options regarding asset and dates selection available.

All reinforcement learning agent training is executed using `main.py` with instructions provided within the file. Upon the completion of each experiment, relevant directories within `results/` titled by the reward dynamic will be created. Inside each will exist directories for data and models with subdirectories titled by the environment name.

Final aggregated figures for all agent experiments that share common training parameters are generated using `plotting/gen_figures.py` where specific aggregation details must be inputted into the file.

Accessing the code involves the following commands:
```commandline
git clone https://codeberg.org/raja-grewal/rlmd.git

cd rlmd
```

Next `fetch` and `checkout` the most up-to-date existing data stored using [Git LFS](https://git-lfs.github.com/) with:
```
git lfs install

git lfs pull
```
Optionally, install and enable the use of [pre-commit](https://pre-commit.com/) for making contributions:
```
pip install pre-commit

pre-commit install
```

Install all required packages (ideally in a virtual environment) without dependencies using:
```commandline
pip install setuptools pip --upgrade

pip install -r requirements--no-deps.txt
```

Optimal leverage roll experiments for a particular "gamble" are conducted with:
```commandline
python lev/gamble.py
```

Reinforcement learning agent training and evaluation is executed with:
```commandline
python main.py
```

There are also additional notes and prerequisites for installing certain parent packages:
* `git-lfs`: Git Large File Storage (LFS) extension must be present. It can be installed [directly](https://github.com/git-lfs/git-lfs) or from relevant repositories.

* `imageio` and `mujoco`: Optional and required only for additive MuJoCo environments.

* `pandas-datareader`: Optional and required only when updating existing or creating new market environments by directly obtaining historical financial market data (prices and volumes).

* `pre-commit`: Optional automated hook script functionality for contributing can be obtained either [directly](https://github.com/pre-commit/pre-commit) or through [PyPI](https://pypi.org/project/pre-commit/).

* `pybullet`: Optional and required only for additive PyBullet environments and requires a C/C++ compiler. On Windows, a [guide](https://wiki.archlinux.org/title/Installation_guide) is available, or alternatively, install [Microsoft Visual Studio Community](https://visualstudio.microsoft.com/) and in the installer select the “Desktop development with C++” workload with both optional features “MSVC v143 - VS 2022 C++ x64/x86 build tools” and the “Windows 10 SDK”.

* `torch`: Installation should be performed following the official [instructions](https://pytorch.org/get-started/locally/).  In terms of compute platforms, the codebase is compatible with any CPU and only NVIDIA GPUs (through CUDA). AMD GPUs (through ROCm) are currently unsupported.

Note only the exact versions shown in `requirements--no-deps.txt` should be utilised as others can significantly reduce speed, lead to broken function/method calls, and cause tests to fail.

Furthermore, in order to minimise overall training time, for example, each instance of agent training on existing single-asset market environments with the default hyperparameters consumes roughly 2.9 GB and 1.1 GB of RAM and VRAM respectively. Therefore, multiple parallel runs on the same GPU using different training configurations are possible and encouraged. Training should be done utilising all available system RAM and per GPU VRAM, with later often being the limiting factor. Overall, this will be significantly faster than the alternative of a single run iterating sequentially over the all the same training configurations.

## Tests

Comprehensive tests during compilation have been written for all user inputs.

A reduced scale functionality test across all optimal leverage experiments is performed with:
```commandline
python tests/test_script_lev.py
```

An initial test for the early stability of agent training across a variety of scenarios can be conducted using:
```commandline
python tests/test_script_agent.py
```

The agent learning will also terminate if critic network backpropagation fails that occurs mainly due to the use of strong outlier-suppressing critic loss functions, divergence in particular environment state components, instability of failed learning for very extended periods of time.

## References

* Landau and Lifshitz primer on statistical mechanics, ensemble averages, and entropy ([1980](https://archive.org/details/landau-and-lifshitz-physics-textbooks-series/Vol%205%20-%20Landau%2C%20Lifshitz%20-%20Statistical%20Physics%20Part%201%20%283rd%2C%201980%29) and [1994]( https://archive.org/details/landau-and-lifshitz-physics-textbooks-series/Vol%202%20-%20Landau%2C%20Lifshitz%20-%20The%20classical%20theory%20of%20fields%20%284th%2C%201994%29/mode/2up))
* Non-i.i.d. data and fat tails ([Fazekas and Klesov 2006](https://epubs.siam.org/doi/pdf/10.1137/S0040585X97978385), [Taleb 2009](https://sciencedirect.com/science/article/abs/pii/S016920700900096X), [Taleb and Sandis 2014](https://arxiv.org/pdf/1308.0a58.pdf), [Cirillo and Taleb 2016](https://tandfonline.com/doi/pdf/10.1080/14697688.2016.1162908?needAccess=true), [Cirillo and Taleb 2020](https://nature.com/articles/s41567-020-0921-x.pdf), [Taleb 2020](https://arxiv.org/ftp/arxiv/papers/2001/2001.10488.pdf), and [Lagnado and Taleb 2022](https://jai.pm-research.com/content/early/2022/02/04/jai.2022.1.157))
* Kelly criterion ([Bernoulli 1738](http://risk.garven.com/wp-content/uploads/2013/09/St.-Petersburg-Paradox-Paper.pdf), [Kelly 1956](https://cpb-us-w2.wpmucdn.com/u.osu.edu/dist/7/36891/files/2017/07/Kelly1956-1uwz47o.pdf), [Ethier 2004](https://cambridge.org/core/journals/journal-of-applied-probability/article/abs/kelly-system-maximizes-median-fortune/DD46B2432B0E251CF2CFFA9E90D31A2B), and [Nekrasov 2013](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=2259133))
* Multiplicative dynamics ([Peters 2011a](https://tandfonline.com/doi/pdf/10.1080/14697688.2010.513338?needAccess=true), [Peters 2011b](https://royalsocietypublishing.org/doi/pdf/10.1098/rsta.2011.0065), [Peters 2011c](https://arxiv.org/pdf/1110.1578.pdf), [Gigerenzer and Brighton 2012](https://ncbi.nlm.nih.gov/pmc/articles/PMC3629675/pdf/mjms-19-4-006.pdf), [Peters and Gell-Mann 2016](https://aip.scitation.org/doi/pdf/10.1063/1.4940236), [Peters 2019](https://nature.com/articles/s41567-019-0732-0.pdf), [Peters et al. 2020](https://arxiv.org/ftp/arxiv/papers/2005/2005.00056.pdf), [Meder et al. 2020](https://arxiv.org/ftp/arxiv/papers/1906/1906.04652.pdf), [Peters and Adamou 2021](https://arxiv.org/pdf/1801.03680.pdf), [Spitznagel 2021](https://wiley.com/en-us/Safe+Haven%3A+Investing+for+Financial+Storms-p-9781119401797), and [Vanhoyweghen et al. 2022](https://nature.com/articles/s41598-022-07613-6.pdf))
* Modelling time series ([Cerqueira et al. 2017](https://ieeexplore.ieee.org/document/8259815), [de Prado 2018](https://wiley.com/en-us/Advances+in+Financial+Machine+Learning-p-9781119482086), and [Cerqueira, Torgo and Mozetič 2020](https://link.springer.com/content/pdf/10.1007/s10994-020-05910-7.pdf))
* Reinforcement learning ([Szepesvári 2009](https://sites.ualberta.ca/~szepesva/papers/RLAlgsInMDPs.pdf) and [Sutton and Bartow 2018](http://incompleteideas.net/book/RLbook2020.pdf))
* Feature reinforcement learning ([Hutter 2009](https://sciendo.com/downloadpdf/journals/jagi/1/1/article-p3.pdf), [Hutter 2016](https://sciencedirect.com/science/article/pii/S0304397516303772), and [Majeed and Hutter 2018](https://ijcai.org/Proceedings/2018/0353.pdf))
* Twin Delayed DDPG (TD3) ([Silver et al. 2014](http://proceedings.mlr.press/v32/silver14.pdf), [Lillicrap et al. 2016](https://arxiv.org/pdf/1509.02971.pdf), and [Fujimoto et al. 2018](https://arxiv.org/pdf/1802.09477.pdf))
* Soft Actor-Critic (SAC) ([Ziebart 2010](https://cs.cmu.edu/~bziebart/publications/thesis-bziebart.pdf), [Haarnoja et al. 2017](http://proceedings.mlr.press/v70/haarnoja17a/haarnoja17a-supp.pdf), and [Haarnoja et al. 2018](https://arxiv.org/pdf/1812.05905.pdf))
* Critic loss functions from NMF ([Guan et al. 2019](https://arxiv.org/pdf/1906.00495.pdf))
* Multi-step returns and replay coupling ([Meng, Gorbet and Kulic 2020](https://arxiv.org/pdf/2006.12692.pdf) and [Fedus et al. 2020](https://arxiv.org/pdf/2007.06700.pdf))
* Power consumption of neural networks ([Han et al. 2015](https://proceedings.neurips.cc/paper/2015/file/ae0eb3eed39d2bcef4622b2499a05fe6-Paper.pdf) and [García-Martín et al. 2019](https://sciencedirect.com/science/article/pii/S0743731518308773))

## Acknowledgements

The base TD3 and SAC algorithms were implemented using guidance from: [DLR-RM/stable-baelines3](https://github.com/DLR-RM/stable-baselines3), [haarnoja/sac](https://github.com/haarnoja/sac), [openai/spinningup](https://github.com/openai/spinningup), [p-christ/Deep-Reinforcement-Learning-Algorithms-with-PyTorch](https://github.com/p-christ/Deep-Reinforcement-Learning-Algorithms-with-PyTorch), [philtabor/Actor-Critic-Methods-Paper-To-Code](https://github.com/philtabor/Actor-Critic-Methods-Paper-To-Code), [rail-berkley/softlearning](https://github.com/rail-berkeley/softlearning), [rlworkgroup/garage](https://github.com/rlworkgroup/garage), and [sfujim/TD3](https://github.com/sfujim/TD3).

If you use any of this work, please cite our results like this:
```bibtex
@misc{jsgrewal2022,
  author        = {J. S. Grewal},
  title         = {Reinforcement Learning in Multiplicative Domains},
  publisher     = {Codeberg},
  journal       = {Codeberg Repository},
  howpublished  = {\url{https://codeberg.org/raja-grewal/rlmd}},
  year          = {2022 - 2023}
  }
```

This repository also utilises the strong copyleft [GNU Affero General Public License v3.0](https://gnu.org/licenses/agpl-3.0.en.html) or later version in order to encourage open source development of all the ideas, applications, and code enclosed within this repository.

Copyright (C) 2022 - 2023 J. S. Grewal (<rg_public@proton.me>).
